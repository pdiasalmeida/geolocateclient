package infoportugal.georeferencia;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements CvCameraViewListener2
{
	private static final String TAG = "Georeferenciador InfoPortugal";
	private IPGeoreferencia app;

	private CameraBridgeViewBase _mOpenCvCameraView;
	private Mat _cRgba;

	String upLoadServerImageUri = "http://192.168.1.109:8000/uploadImage";
	String upLoadServerFeatureUri = "http://192.168.1.109:8000/uploadFeatures";
	int serverResponseCode = 0;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this)
	{
		@Override
		public void onManagerConnected(int status)
		{
			switch(status)
			{
			case LoaderCallbackInterface.SUCCESS:
			{
				Log.i( TAG, "OpenCV loaded successfully" );					
				_mOpenCvCameraView.enableView();
			} break;
			default:
			{
				super.onManagerConnected(status);
			} break;
			}
		}
	};

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		Log.i( TAG, "called onCreate" );
		super.onCreate( savedInstanceState );

		app = (IPGeoreferencia) getApplication();

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView( R.layout.activity_main );

		_mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
		_mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		_mOpenCvCameraView.setCvCameraViewListener(this);		

		final Button button = (Button) findViewById(R.id.upload_button);
		button.setOnClickListener( new View.OnClickListener()
		{
			public void onClick( View v )
			{
				process(_cRgba);
			}
		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		OpenCVLoader.initAsync( OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback );
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if( _mOpenCvCameraView != null )
		{
			_mOpenCvCameraView.disableView();
		}
	}

	public void onDestroy()
	{
		super.onDestroy();
		if( _mOpenCvCameraView != null )
		{
			_mOpenCvCameraView.disableView();
		}
	}

	@Override
	public void onCameraViewStarted( int width, int height ) {}

	@Override
	public void onCameraViewStopped() {}

	@Override
	public Mat onCameraFrame( CvCameraViewFrame inputFrame )
	{
		_cRgba = inputFrame.rgba();

		return _cRgba;
	}

	public void processTest( Mat image )
	{
		Bitmap bimage = Bitmap.createBitmap( image.cols(), image.rows(), Bitmap.Config.ARGB_8888 );
		Utils.matToBitmap( image, bimage );
		app.results = bimage;

		Intent intent = new Intent( MainActivity.this, DisplayResults.class );		
		startActivity(intent);
	}

	public void process( Mat image )
	{
		final Mat timage = image;
		new Thread() {
			@Override
			public void run() {
				uploadFile(timage);
			}
		}.start(); 
	}
	
	public void processF( Mat image )
	{
		JSONArray features = new JSONArray();
		char f1 = (char)255;features.put(f1);
		char f2 = (char)254;features.put(f2);
		char f3 = (char)253;features.put(f3);
		char f4 = (char)252;features.put(f4);
		char f5 = (char)251;features.put(f5);
		char f6 = (char)250;features.put(f6);
		char f7 = (char)249;features.put(f7);
		char f8 = (char)248;features.put(f8);
		char f9 = (char)247;features.put(f9);
		char f10 = (char)246;features.put(f10);
		
		uploadJSON(features);
	}

	public int uploadFile( Mat image )
	{
		String fileName = "test.png";

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		// read bitmap to byte array
		Bitmap bimage = Bitmap.createBitmap( image.cols(), image.rows(), Bitmap.Config.ARGB_8888 );
		Utils.matToBitmap( image, bimage );
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bimage.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
		byte [] byte_arr = stream.toByteArray();

		ByteArrayInputStream byteInputStream = new ByteArrayInputStream(byte_arr);

		try { // open a URL connection to the Servlet
			URL url = new URL(upLoadServerImageUri);
			conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			
			dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes( twoHyphens + boundary + lineEnd );
			dos.writeBytes( "Content-Disposition: form-data; name=\"files\";filename=\""+ fileName + "\"" + lineEnd );
			dos.writeBytes( lineEnd );

			bytesAvailable = byteInputStream.available(); // create a buffer of  maximum size

			bufferSize = Math.min( bytesAvailable, maxBufferSize );
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = byteInputStream.read( buffer, 0, bufferSize );

			while( bytesRead > 0 )
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = byteInputStream.available();
				bufferSize = Math.min( bytesAvailable, maxBufferSize );
				bytesRead = byteInputStream.read( buffer, 0, bufferSize );
			}

			// send multipart form data necessary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			final String serverResponseMessage = conn.getResponseMessage();
			
			InputStream loc_is = conn.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(loc_is));
			StringBuilder total = new StringBuilder();
			String line;
			while( (line = r.readLine()) != null )
			{
			    total.append(line);
			}
			final String location = total.toString();

			Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
			if( serverResponseCode == 200 )
			{
				MainActivity.this.runOnUiThread(new Runnable()
				{
					public void run()
					{
						Log.i( "Location computed", location );
						Toast.makeText(MainActivity.this, "Location computed: " + location,
								Toast.LENGTH_LONG).show();
					}
				});
			}

			//close the streams //
			byteInputStream.close();
			dos.flush();
			dos.close();

		}
		catch( MalformedURLException ex )
		{
			ex.printStackTrace();
			MainActivity.this.runOnUiThread( new Runnable()
			{
				public void run()
				{
					Toast.makeText(MainActivity.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		}
		catch (Exception e)
		{
			final Exception te = e;
			e.printStackTrace();
			MainActivity.this.runOnUiThread(new Runnable()
			{
				public void run()
				{
					Toast.makeText(MainActivity.this, "Exception : " + te.getMessage(), Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
		}

		return serverResponseCode;  
	}
	
	public void uploadJSON( final JSONArray features )
	{
		Thread t = new Thread()
		{
			public void run()
			{
				Looper.prepare(); //For Preparing Message Pool for the child Thread
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject json = new JSONObject();

                try
                {
                    HttpPost post = new HttpPost(upLoadServerFeatureUri);
                    json.put("features", features);
                    StringEntity se = new StringEntity( json.toString());  
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    /*Checking response */
                    if( response != null )
                    {
                        InputStream in = response.getEntity().getContent(); //Get the data in the entity
                    }

                }
                catch( Exception e ) 
                {
                	e.printStackTrace();
                    final Exception te = e;
                    MainActivity.this.runOnUiThread( new Runnable()
                    {
        				public void run()
        				{
        					Toast.makeText(MainActivity.this, "Exception : " + te.getMessage(), Toast.LENGTH_SHORT).show();
        				}
        			});
                }

                Looper.loop(); //Loop in the message queue
            }
        };

        t.start();
	}

}
